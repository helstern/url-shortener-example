url-shortener
=============

This is a simple url shortener application which creates shortened urls over a rest interface.
For each URL entered, the server adds a new alias in its hashed database and returns the shortened url such as http://192.168.33.10/5unsh in the response. 

If the URL has already been requested, it should return the existing alias rather than create a new one. 
The short URL forwards users to the long URL.
The rest interface is basic, without any sort of content negotiation, the representations are plain text.

To create an url, run the following command from your host machine:
    
    curl -d "<your url>" -XPOST http://192.168.33.10

The response body is the shortened url

To request a redirect (from your host machine)
    
    curl http://192.168.33.10/<alias>

In almost every case the api returns sensible error codes. When an error occurs, the error message is the entire body of the response

Glossary of terms
---

Source Url = the url which is shortened

Alias = the shortened identifier

Installation
----

the project requires virtualbox and vagrant. 

on the host machine, cd into the dir of the project and run 
    
    make vagrant
    
this will download the scotch.io box, start a new virtual machine and update system packages.

after bootstrap, still on the host machine run       
    
    cd vagrant; vagrant ssh
    
you are now inside the virtual machine and you must install the app. the app is located at /var/www
    
    cd /var/www; make app


Tests
----

from inside the virtual machine, run    
    
    cd /var/www; make test   
   
Implementation details
----

the solution considered that the service is backed by a single mysql database. no distributed databases or advanced distributed id 
generation strategies were considered.

a clear distinction has been made between the Domain Model and the Persistence Model (ORM), to allow each one to evolve independently.
object to object mapping is used to convert between the objects of the two models

a clear distinction has been made between the Domain Services and the Application Services, with the Domain Services handling 
validation and Application Services converting between the Domain and Persistence Models and providing support

Generating the url alias
----

the alias is obtained by monotonically incrementing a counter stored in a mysql innodb table then base 36 encoding it to obtain 
an identifier. using base 36 encoding a large number like 1 billion (1.000.000.000) can be stored in 6 letter.

the key to reliably process urls is to make sure that each url gets assigned a non repetable identifier. two alias generation
strategies are provided, one that locks the entire table while generating the next one, and what that uses row locking, both pessimistic.

the schema for the alias table looks like this:
    
    +---------+---------+-----+----------------+
    | Field   | Type    | Key | Extra          |
    +---------+---------+-----+----------------+
    | id      | int(11) | PRI | auto_increment |
    | counter | int(11) |     |                |
    +---------+---------+-----+----------------+


the table contains only one row at the moment, but because there is a primary key we can add more counters if performance
degrades using one counter.
