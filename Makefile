SHELL=/bin/bash

app: download composer database database-test

init: download

vagrant:
	cd vagrant; vagrant up

download:
	if ! test -d bin; then mkdir bin; fi
	rm -rf bin/symfony; curl -LsS https://symfony.com/installer -o bin/symfony && chmod a+x bin/symfony
	rm -rf bin/composer.phar; curl -sS https://getcomposer.org/composer.phar -o bin/composer.phar && chmod a+x bin/composer.phar

composer:
	php bin/composer.phar --no-interaction install

database:
	php bin/console doctrine:database:create
	php bin/console doctrine:schema:create
	php bin/console doctrine:fixtures:load --no-interaction --purge-with-truncate

test:
	php vendor/bin/phpunit

database-test:
	php bin/console doctrine:database:create --env test
	php bin/console doctrine:schema:create --env test
	php bin/console doctrine:fixtures:load --env test --no-interaction --purge-with-truncate

clean:
	php bin/console doctrine:database:drop --force
	rm -rf vendor
	rm -rf var/cache/dev/*
	rm -rf var/cache/test/*
	rm -rf var/cache/prod/*



.PHONY: vagrant composer database database-test clean init app test
