<?php namespace AppBundle\Service;

use Doctrine\ORM;
use AppBundle\Entity as Entity;
use Doctrine\ORM\EntityManagerInterface;

class MysqlLockTableHashGenerator implements SqlHashGenerationStrategy
{
    /**
     * {@inheritdoc}
     */
    function generate(EntityManagerInterface $em)
    {
        $nextValue = $this->generateNextHash($em);
        if (is_null($nextValue)) {
            return null;
        }

        return base_convert($nextValue, 10, 36);
    }

    /**
     * This method generates and allocates the next url number by locking the entire table
     *
     * @param EntityManagerInterface $em
     *
     * @return int
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    private function generateNextHash(EntityManagerInterface $em)
    {
        $metadata = $em->getClassMetadata(Entity\Counter::class);
        $table = $metadata->getTableName();
        $column = $metadata->getColumnName('counter');
        $nextValue = null;

        $connection = $em->getConnection();
        try {
            $connection->executeQuery(sprintf('LOCK TABLES `%s` WRITE', $table));
            $connection->executeQuery(sprintf('UPDATE `%s` set `%s` = `%s` + 1', $table, $column, $column));
            $nextValue = $connection->fetchColumn(sprintf('SELECT `%s` FROM `%s` LIMIT 1', $column, $table));
            $connection->executeQuery('COMMIT');
            $connection->executeQuery('UNLOCK TABLES');
        } catch (\Exception $e) {
            die ($e->getMessage());
        }

        if (! is_null($nextValue)) {
            return (int) $nextValue;
        }

        return null;
    }
}
