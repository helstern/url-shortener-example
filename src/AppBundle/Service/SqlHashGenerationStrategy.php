<?php namespace AppBundle\Service;

use Doctrine\ORM as ORM;

interface SqlHashGenerationStrategy
{
    /**
     * @param \Doctrine\ORM\EntityManagerInterface $em
     *
     * @return string|null
     */
    function generate(ORM\EntityManagerInterface $em);
}
