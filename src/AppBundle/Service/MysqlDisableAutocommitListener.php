<?php namespace AppBundle\Service;

use Doctrine\DBAL\Event\ConnectionEventArgs;
use Doctrine\DBAL\Events;
use Doctrine\Common\EventSubscriber;

class MysqlDisableAutocommitListener implements EventSubscriber
{
    /**
     * @param ConnectionEventArgs $args
     *
     * @return void
     */
    public function postConnect(ConnectionEventArgs $args)
    {
        $query = 'SET autocommit = 0';
        $args->getConnection()->executeUpdate($query);
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return array(Events::postConnect);
    }
}
