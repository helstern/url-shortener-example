<?php namespace AppBundle\Service;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM;
use AppBundle\Entity as Entity;
use Doctrine\ORM\EntityManagerInterface;

class PessimisticLockHashGenerator implements SqlHashGenerationStrategy
{
    function generate(EntityManagerInterface $em)
    {
        $nextValue = $this->generateNextHash($em);
        if (is_null($nextValue)) {
            return null;
        }

        return base_convert($nextValue, 10, 36);
    }

    /**
     * This method generates and allocates the next url hash
     *
     * @param EntityManagerInterface $em
     *
     * @return string
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    private function generateNextHash(EntityManagerInterface $em)
    {
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            /** @var Entity\Counter $counter */
            $counter = $em
                ->createQuery('SELECT c FROM AppBundle:Counter c')
                ->setMaxResults(1)
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getSingleResult();

            $nextValue = $counter->getCounter() + 1;

            $q = $em->createQueryBuilder()->update('AppBundle:Counter', 'c')
                ->set('c.counter', $nextValue)
                ->where('c.id = :id')
                ->andWhere('c.counter = :previousValue')
                ->setParameter('id', $counter->getId())
                ->setParameter('previousValue', $counter->getCounter())
                ->getQuery();
            $rows = $q->execute();

            $em->flush();
            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        if ($rows != 1) {
            return null;
        }

        return $nextValue;
    }
}
