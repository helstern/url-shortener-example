<?php namespace AppBundle\Service;

use AppBundle\Domain as Domain;
use AppBundle\Entity as Entity;
use Doctrine\ORM as ORM;
use Symfony\Component\Validator\Constraints as Constraints ;

class UrlService
{
    /**
     * @var SqlHashGenerationStrategy
     */
    private $hashGenerationStrategy;

    /**
     * @var ORM\EntityManagerInterface
     */
    private $em;

    /**
     * @param \AppBundle\Service\SqlHashGenerationStrategy $hashGenerationStrategy
     * @param \Doctrine\ORM\EntityManagerInterface $em
     */
    public function __construct(
        SqlHashGenerationStrategy $hashGenerationStrategy,
        ORM\EntityManagerInterface $em
    ) {
        $this->hashGenerationStrategy = $hashGenerationStrategy;
        $this->em = $em;
    }

    /**
     * Find a source url by alias
     *
     * @param string $alias
     *
     * @return bool
     */
    public function findSourceUrlByAlias($alias)
    {
        $result = $this->em
            ->createQuery(
                'SELECT c FROM AppBundle:Url c where c.alias = :alias'
            )
            ->setParameter('alias', $alias)
            ->getOneOrNullResult();

        if (is_null($result)) {
            return null;
        }

        return $result->getSourceUrl();
    }


    /**
     * Check if an alias has been generated for $sourceUrl
     *
     * @param Domain\SourceUrl $sourceUrl
     *
     * @return bool
     */
    public function aliasExists(Domain\SourceUrl $sourceUrl)
    {
        return false;
    }

    /**
     * @param Domain\SourceUrl $sourceUrl
     *
     * @return Domain\Alias
     * @throws CreateAliasException
     */
    public function createAlias(Domain\SourceUrl $sourceUrl)
    {
        if (! $this->aliasExists($sourceUrl)) {
            try {
                $hash = $this->hashGenerationStrategy->generate($this->em);
            } catch (\Exception $e) {
                throw new CreateAliasException('failed to generate hash ', 0, $e);
            }

            if (is_null($hash)) {
                throw new CreateAliasException('failed to generate hash');
            }

            $alias = new Domain\Alias($hash, $sourceUrl);
            return $alias;
        }

        //todo: implement proper retrieval of
        throw new CreateAliasException();
    }

    /**
     * Returns the persistence id of $alias
     *
     * @param Domain\Alias $alias
     *
     * @return int
     * @throws UrlServiceException
     */
    public function persistAlias(Domain\Alias $alias)
    {
        $entity = new Entity\Url();
        //normally i would use an object to object mapper like michelsalib/BCCAutoMapperBundle or rezonant/mapper-bundle
        //instead of doing this manually
        $this->mapDomainToEntity($alias, $entity);

        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            $this->em->persist($entity);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            throw new UrlServiceException('failed to persist alias', 0, $e);
        }

        return $entity->getId();
    }

    /**
     * @param Domain\Alias $alias
     * @param \AppBundle\Entity\Url $entity
     *
     * @return void
     */
    private function mapDomainToEntity(Domain\Alias $alias, Entity\Url $entity)
    {
        $hash = $alias->getHash();
        $entity->setAlias($hash);

        $checksum = $alias->getChecksum();
        $entity->setChecksum($checksum);

        $sourceUrl = $alias->getSourceUrl()->toUrlString();
        $entity->setSourceUrl($sourceUrl);
    }
}
