<?php namespace AppBundle\Controller;

use AppBundle\Domain as Domain;
use AppBundle\Service as Services;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Routing;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Routing\Route(service="default_controller")
 */
class DefaultController
{
    /**
     * @var Services\UrlService
     */
    private $appUrlService;

    /**
     * @var Domain\UrlService
     */
    private $domainUrlService;

    /**
     * @param Services\UrlService $urlService
     * @param Domain\UrlService $aliasHost
     */
    public function __construct(Services\UrlService $urlService, Domain\UrlService $aliasHost)
    {
        $this->appUrlService = $urlService;
        $this->domainUrlService = $aliasHost;
    }

    /**
     * @Routing\Route("/", name="index")
     * @Routing\Method("GET")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return new Response('Missing alias', Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Routing\Route("/{alias}", name="alias")
     * @Routing\Method("GET")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function aliasAction(Request $request, $alias)
    {
        if (empty($alias)) {
            return new Response('Missing alias', Response::HTTP_BAD_REQUEST);
        }

        $sourceUrl = $this->appUrlService->findSourceUrlByAlias($alias);
        if (is_null($sourceUrl)) {
            return new Response('Alias not found', Response::HTTP_NOT_FOUND);
        }

        return new RedirectResponse($sourceUrl, 302);
    }

    /**
     * @Routing\Route("/", name="create-url")
     * @Routing\Method("POST")
     * @param Request $request
     *
     * @return Response
     */
    public function createUrlAction(Request $request)
    {
        $urlString = $request->getContent();

        try {
            $sourceUrl = $this->domainUrlService->createSourceUrl($urlString);
        } catch (Domain\BuildUrlException $e) {
            return new Response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        //here we try to create alias and allocate it's unique number
        try {
            $alias = $this->appUrlService->createAlias($sourceUrl);
        } catch (Services\UrlServiceException $e) {
            return new Response('service is unavailable. try again later 1', Response::HTTP_SERVICE_UNAVAILABLE);
        }

        //if we have already the unique number but we fail to save it we might retry with the same unique number
        // and that's why we handle this separately.
        try {
            $this->appUrlService->persistAlias($alias);
        } catch (Services\UrlServiceException $e) {
            return new Response('service is unavailable. try again later 2', Response::HTTP_SERVICE_UNAVAILABLE);
        }

        $url = $this->domainUrlService->toHostUrl($alias);
        return new Response($url, Response::HTTP_OK);
    }
}
