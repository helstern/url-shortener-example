<?php namespace AppBundle\Domain;

class SourceUrl
{
    /**
     * @var string
     */
    private $protocol;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $path;

    /**
     * @var array
     */
    private $query;

    /**
     * @var string
     */
    private $fragment;

    /**
     * @param string $protocol
     * @param string $host
     * @param string $path
     * @param array $query
     * @param string $fragment
     */
    public function __construct($protocol, $host, $path, array $query, $fragment)
    {
        $this->protocol = $protocol;
        $this->host = $host;
        $this->path = $path;
        $this->query = $query;
        $this->fragment = $fragment;
    }

    /**
     * Returns the string representation of this url
     *
     * @return string
     */
    public function toUrlString()
    {
        $string = $this->protocol . '://' . $this->host . $this->path;

        //let's not touch the original map
        if (! empty($this->query)) {
            $string .= '?' . http_build_query($this->query, null, '&');
        }

        if (! empty($this->fragment)) {
            $string .= '#' . $this->fragment;
        }

        return $string;
    }

    /**
     * Returns a normalized string representation of this url
     *
     * @return string
     */
    public function toNormalizedUrlString()
    {
        $normalized = strtolower($this->protocol) . '://' . strtolower($this->host) . $this->path;

        //let's not touch the original map
        if (! empty($this->query)) {
            $query = $this->query;
            ksort($query);
            $normalized .= '?' . http_build_query($query, null, '&');
        }

        if (! empty($this->fragment)) {
            $normalized .= '#' . $this->fragment;
        }

        return $normalized;
    }

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return string
     */
    public function getFragment()
    {
        return $this->fragment;
    }

    /**
     * @param \AppBundle\Domain\SourceUrl $other
     *
     * @return bool
     */
    public function equals(SourceUrl $other)
    {
        $normalizedOtherString = $other->toNormalizedUrlString();
        if ($this->toNormalizedUrlString() == $normalizedOtherString) {
            return true;
        }

        return false;
    }
}
