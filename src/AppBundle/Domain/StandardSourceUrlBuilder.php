<?php namespace AppBundle\Domain;

use Symfony\Component\Validator\Constraints as Constraints;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * This class is one possible implementation of a url builder which only applies specific constraints on the url
 *
 *
 * @package AppBundle\Domain
 */
class StandardSourceUrlBuilder implements UrlBuilder
{
    /** @var ValidatorInterface*/
    private $validator;

    /**
     * @var string
     * @Constraints\NotBlank()
     */
    private $protocol;

    /**
     * @var string
     * @Constraints\NotBlank()
     */
    private $host;

    /**
     * @var string
     */
    private $path = '/';

    /**
     * @var array
     */
    private $query = [];

    /**
     * @var string
     */
    private $fragment = '';

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }


    /**
     * @param string $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = '/' . ltrim($path, '/');
    }

    /**
     * @param array $query
     */
    public function setQuery(array $query)
    {
        $this->query = $query;
    }

    /**
     * @param string $fragment
     */
    public function setFragment($fragment)
    {
        $this->fragment = $fragment;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $violations = $this->validator->validate($this);
        if (0 == $violations->count()) {
            return true;
        }

        return false;
    }

    /**
     * @return SourceUrl
     * @throws BuildUrlException
     */
    public function buildSourceUrl()
    {
        if (! $this->isValid()) {
            throw new BuildUrlException('Invalid url');
        }

        $url = new SourceUrl(
            $this->protocol,
            $this->host,
            $this->path,
            $this->query,
            $this->fragment
        );
        return $url;
    }

    public function build()
    {
        $sourceUrl = $this->buildSourceUrl();
        return $sourceUrl->toNormalizedUrlString();
    }
}



