<?php namespace AppBundle\Domain;

class Alias
{
    /**
     * @var $hash
     */
    private $hash;

    /**
     * @var SourceUrl
     */
    private $sourceUrl;

    /**
     * @param string $hash
     * @param SourceUrl $sourceUrl
     */
    public function __construct($hash, SourceUrl $sourceUrl)
    {
        $this->hash = $hash;
        $this->sourceUrl = $sourceUrl;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @return string
     */
    public function getChecksum()
    {
        $normalizedString = $this->sourceUrl->toNormalizedUrlString();
        return md5($normalizedString);
    }

    /**
     * @return SourceUrl
     */
    public function getSourceUrl()
    {
        return $this->sourceUrl;
    }
}
