<?php namespace AppBundle\Domain;

interface UrlBuilder
{
    /**
     * @param string $protocol
     */
    public function setProtocol($protocol);

    /**
     * @param string $host
     */
    public function setHost($host);

    /**
     * @param string $path
     */
    public function setPath($path);

    /**
     * @param array $query
     */
    public function setQuery(array $query);

    /**
     * @param string $fragment
     */
    public function setFragment($fragment);

    /**
     * @return bool
     */
    public function isValid();

    /**
     * @return string
     * @throws BuildUrlException
     */
    public function build();
}



