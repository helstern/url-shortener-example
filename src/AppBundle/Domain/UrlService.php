<?php namespace AppBundle\Domain;

use Symfony\Component\Validator\Constraints as Constraints ;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UrlService
{
    /**
     * @var string
     */
    private $aliasHost;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param string $aliasHost the host which resolves the alias to its source url
     * @param ValidatorInterface $validator
     */
    public function __construct($aliasHost, ValidatorInterface $validator)
    {
        $this->aliasHost = $aliasHost;
        $this->validator = $validator;
    }

    /**
     * @param string $urlString
     *
     * @return bool
     */
    public function validateSyntax($urlString)
    {
        $constraints = [
            new Constraints\NotBlank(),
            new Constraints\Url()
        ];

        $violations = $this->validator->validate($urlString, $constraints);
        if (0 == $violations->count()) {
            return true;
        }

        return false;
    }

    /**
     * @param string $urlString
     *
     * @return SourceUrl
     * @throws BuildUrlException
     */
    public function createSourceUrl($urlString)
    {
        //todo: later we must add clear error messages to the exception for error handling, logging, etc
        if (!$this->validateSyntax($urlString)) {
            throw new BuildUrlException('Invalid url syntax');
        }

        $builder = new StandardSourceUrlBuilder($this->validator);
        $urlParts = $this->parseUrl($urlString);
        $this->configureUrlBuilder($builder, $urlParts);
        return $builder->buildSourceUrl();

    }

    /**
     * @param string $urlString
     *
     * @see parse_url
     * @return array
     */
    private function parseUrl($urlString)
    {
        //parse url for now ignore some components like port username etc
        $urlParts = parse_url($urlString);

        $query = [];
        if (array_key_exists('query', $urlParts) && ! empty($urlParts['query'])) {
            parse_str($urlParts['query'], $query);
        }

        $urlParts['query'] = $query;
        return $urlParts;
    }

    /**
     * @param UrlBuilder $builder
     * @param array $urlParts
     *
     * @return SourceUrl
     * @throws BuildUrlException
     */
    private function configureUrlBuilder(UrlBuilder $builder, array $urlParts)
    {
        if (array_key_exists('scheme', $urlParts)) {
            $builder->setProtocol($urlParts['scheme']);
        }

        if (array_key_exists('host', $urlParts)) {
            $builder->setHost($urlParts['host']);
        }

        if (array_key_exists('path', $urlParts)) {
            $builder->setPath($urlParts['path']);
        }

        if (array_key_exists('query', $urlParts)) {
            $builder->setQuery($urlParts['query']);
        }

        if (array_key_exists('fragment', $urlParts)) {
            $builder->setFragment($urlParts['fragment']);
        }

        return $builder->isValid();
    }

    /**
     * @param Alias $alias
     *
     * @return string
     */
    public function toHostUrl(Alias $alias)
    {
        $builder = new StandardSourceUrlBuilder($this->validator);

        $builder->setHost($this->aliasHost);
        $builder->setProtocol('http');

        $path = $alias->getHash();
        $builder->setPath($path);
        return $builder->build();
    }
}
