<?php namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Counter;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BlogFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $counter = new Counter(1, 1);
        $manager->persist($counter);
        $manager->flush();
    }

}
