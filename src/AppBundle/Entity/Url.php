<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Url
 *
 * @ORM\Table(
 *  name="url",
 *  indexes={
 *      @ORM\Index(name="checksum_idx", columns={"checksum"})
 *  },
 *  uniqueConstraints={
 *      @ORM\UniqueConstraint(name="alias_idx",columns={"alias"}),
 *  },
 * )
 * @ORM\Entity()
 */
class Url
{
    /**
     * @var integer the surrogate key
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string the business key
     *
     * @ORM\Column(name="alias", type="string", length=7, nullable=false)
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="text", nullable=false)
     */
    private $sourceUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="checksum", type="string", length=100, nullable=false)
     */
    private $checksum;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getSourceUrl()
    {
        return $this->sourceUrl;
    }

    /**
     * @param string $sourceUrl
     */
    public function setSourceUrl($sourceUrl)
    {
        $this->sourceUrl = $sourceUrl;
    }

    /**
     * @return string
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

    /**
     * @param string $checksum
     */
    public function setChecksum($checksum)
    {
        $this->checksum = $checksum;
    }
}
