<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Counter
 *
 * @ORM\Table(
 *  name="counter"
 * )
 * @ORM\Entity()
 */
class Counter
{
    /**
     * @var integer the surrogate key
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string the business key
     *
     * @ORM\Column(name="counter", type="integer", length=11, nullable=false)
     */
    private $counter;

    public function __construct($id, $counter)
    {
        $this->id = $id;
        $this->counter = $counter;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCounter()
    {
        return $this->counter;
    }

}


