<?php namespace Tests\AppBundle\Service;

use AppBundle\Domain\SourceUrl;
use AppBundle\Service\PessimisticLockHashGenerator;
use AppBundle\Service\UrlService;
use Doctrine\ORM as ORM;

class UrlServiceTest extends \PHPUnit_Framework_TestCase
{

    public function testCreateAliasWithBase36EncodedHash()
    {
        $expected = base_convert(10, 10, 36);

        $hashGenerationStrategy = $this->getMockBuilder(PessimisticLockHashGenerator::class)->setMethods(['generate'])->getMock();
        $hashGenerationStrategy->expects($this->atLeastOnce())->method('generate')->will($this->returnValue($expected));

        /** @var ORM\EntityManagerInterface $entityManager */
        $entityManager = $this->getMockForAbstractClass(ORM\EntityManagerInterface::class);
        $service = new UrlService($hashGenerationStrategy, $entityManager);

        $sourceUrl = new SourceUrl('http', 'www.google.com', '/', [], '');
        $alias = $service->createAlias($sourceUrl);
        $actual = $alias->getHash();

        $this->assertEquals($expected, $actual);
    }
}
