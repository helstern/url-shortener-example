<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\ArrayInput;

class DefaultControllerTest extends WebTestCase
{
    public function testRequestRedirectWithoutAlias()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testRedirectNotFound()
    {
        $expectedResponse = '2';
        $sourceUrl = 'http://www.google.com?a=b';

        $client = static::createClient();
        $crawler = $client->request('POST', '/', [], [], [], $sourceUrl);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $alias = '1' . $client->getResponse()->getContent();

        $client = static::createClient();
        $crawler = $client->request('GET', "/$alias");
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testRedirectWithAlias()
    {
        $sourceUrl = 'http://www.google.com/?a=c&b='.uniqid();

        $client = static::createClient();
        $crawler = $client->request('POST', '/', [], [], [], $sourceUrl);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $alias = $client->getResponse()->getContent();

        $this->assertNotEmpty($alias);
        $urlParts = parse_url($alias);

        $client = static::createClient();
        $crawler = $client->request('GET', $urlParts['path']);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertEquals($sourceUrl, $client->getResponse()->headers->get('location'));
    }

    public function setUp()
    {
        $kernel = new \AppKernel("test", true);
        $kernel->boot();
        $app = new Application($kernel);
        $app->setAutoExit(false);
        $this->runConsole($app, "doctrine:schema:drop", array("--force" => true));
        $this->runConsole($app, "doctrine:schema:create");
        $this->runConsole($app, "doctrine:fixtures:load", array('--no-interaction' => '', '--purge-with-truncate' => ''));
    }

    protected function runConsole(Application $app, $command, Array $options = array())
    {
        $options["-e"] = "test";
        $options["-q"] = null;
        $options = array_merge($options, array('command' => $command));
        return $app->run(new ArrayInput($options));
    }


    public function testCreate()
    {
        $expectedResponse = 'http://192.168.33.10/2';
        $sourceUrl = 'http://www.google.com?a=b';

        $client = static::createClient();
        $crawler = $client->request('POST', '/', [], [], [], $sourceUrl);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($expectedResponse, $client->getResponse()->getContent());
    }
}
