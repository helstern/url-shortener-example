<?php namespace Tests\AppBundle\Domain;

use AppBundle\Domain\SourceUrl;

class SourceUrlTest extends \PHPUnit_Framework_TestCase
{
    public function testToNormalizedStringIsConsistent()
    {
        $protocol = 'http';
        $host = 'www.google.com';
        $path = '/';
        $query = [
            'a' => 'B',
            'b' => 'A'
        ];
        $fragment = 'hi';

        $naturalOrderString = (new SourceUrl($protocol, $host, $path, $query, $fragment))->toNormalizedUrlString();
        $reversedOrderString = (new SourceUrl($protocol, $host, $path, array_reverse($query, true), $fragment))->toNormalizedUrlString();

        $this->assertEquals($naturalOrderString, $reversedOrderString, 'toNormalizedString is not consistent');
    }

    public function testToUrlStringIsConsistent()
    {
        $expectedString = 'http://www.google.com/?b=A&a=B#hi';

        $protocol = 'http';
        $host = 'www.google.com';
        $path = '/';
        $query = [
            'b' => 'A',
            'a' => 'B'
        ];
        $fragment = 'hi';

        $urlString = (new SourceUrl($protocol, $host, $path, $query, $fragment))->toUrlString();
        $normalizedUrlString = (new SourceUrl($protocol, $host, $path, $query, $fragment))->toNormalizedUrlString();

        $this->assertEquals($expectedString, $urlString);
        $this->assertNotEquals($expectedString, $normalizedUrlString);
    }
}
