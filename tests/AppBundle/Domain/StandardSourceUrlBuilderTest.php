<?php namespace Tests\AppBundle\Domain;

use AppBundle\Domain\BuildUrlException;
use AppBundle\Domain\StandardSourceUrlBuilder;
use Symfony\Component\Validator\Validation;

class StandardSourceUrlBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testStandardUrlBuilderCanNotBuildWithoutRequiredProperties()
    {
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $actualException = null;
        try {
            $builder = new StandardSourceUrlBuilder($validator);
            $builder->build();
        } catch (BuildUrlException $e) {
            $actualException = $e;
        }
        $this->assertNotEmpty($actualException, 'should not build a');


        $actualException = null;
        try {
            $builder = new StandardSourceUrlBuilder($validator);
            $builder->setProtocol('http');
            $builder->build();
        } catch (BuildUrlException $e) {
            $actualException = $e;
        }
        $this->assertNotEmpty($actualException, 'should not build b');

        $actualException = null;
        try {
            $builder = new StandardSourceUrlBuilder($validator);
            $builder->setHost('google.com');
            $builder->build();
        } catch (BuildUrlException $e) {
            $actualException = $e;
        }
        $this->assertNotEmpty($actualException, 'should not build c');
    }

    public function testCanBuildUrl()
    {
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $builder = new StandardSourceUrlBuilder($validator);

        $builder->setProtocol('http');
        $builder->setHost('google.com');
        $builder->setPath('/me');
        $builder->setQuery(['a' => 'b', 'c' => 'd']);
        $builder->setFragment('a');

        $expected = 'http://google.com/me?a=b&c=d#a';
        $actual = $builder->build();

        $this->assertEquals($expected, $actual);
    }
}
