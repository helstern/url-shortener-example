<?php namespace Tests\AppBundle\Domain;

use AppBundle\Domain\Alias;
use AppBundle\Domain\BuildUrlException;
use AppBundle\Domain\SourceUrl;
use AppBundle\Domain\UrlService;
use Symfony\Component\Validator\Validation;

class UrlServiceTest extends \PHPUnit_Framework_TestCase
{

    public function testCannotCreateSourceUrlFromInvalidUrl()
    {
        $actualException = null;
        $validator = Validation::createValidator();
        $service = new UrlService('127.0.0.1', $validator);
        try {
            $invalidUrl = 'not an url';
            $service->createSourceUrl($invalidUrl);
        } catch (BuildUrlException $e) {
            $actualException = $e;
        }

        $this->assertNotNull($actualException);
    }

    public function testBuildHostUrl()
    {
        $sourceUrl = new SourceUrl('http', 'www.google.com', '/', [], '');
        $alias = new Alias('eder1v', $sourceUrl);

        $validator = Validation::createValidator();
        $service = new UrlService('127.0.0.1', $validator);
        $hostUrl = $service->toHostUrl($alias);

        $this->assertEquals('http://127.0.0.1/eder1v', $hostUrl);
    }

}
